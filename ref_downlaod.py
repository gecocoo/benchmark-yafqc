import re
import sys
import os
import os.path
import gzip
import shutil
from concurrent.futures import ThreadPoolExecutor
import concurrent
from tqdm import tqdm

reg = re.compile("ftp:\S+")


def get_ftp_links(path):
    if not os.path.isfile(path[:-4] + "_ftp.txt"):
        with open(path) as f:
            with open(path[:-4] + "_ftp.txt", "a") as f2:
                for line in tqdm(f):
                    if ("Complete Genome" in line) and ("latest" in line):
                        part = reg.search(line)[0]
                        f2.write(part + part[part.rfind("/") :] + "_genomic.fna.gz\n")


def download_ftp_files(path):
    current_directory = os.getcwd()
    final_directory = os.path.join(current_directory, path)
    if not os.path.exists(final_directory):
        os.makedirs(final_directory)
        with ThreadPoolExecutor(max_workers=16) as ex:
            futures = {
                ex.submit(download_files, url, path): url
                for url in read_file(path + "_ftp.txt")
            }
            with tqdm(total=len(futures)) as pbar:
                for future in tqdm(concurrent.futures.as_completed(futures)):
                    # futures[future]
                    try:
                        _ = future.result()
                        pbar.update(1)
                    except:
                        print("error")

        # Download all files from _ftp file
        # os.system("wget  -P " + path + " -i " + path + "_ftp.txt")
        # with open(path+"_ftp.txt") as f:
        #        for line in f:
        #            print(path+"_ftp.txt")
        #            wget.download(line[:-1],path)


def unzip_files(path):
    current_directory = os.getcwd()
    file_type = ".gz"
    print(current_directory + path)
    with open(current_directory + path + ".fna", "wb") as f_out:
        for fname in tqdm(os.listdir(current_directory + path)):
            if fname.endswith(file_type):
                with open(
                    os.path.join(current_directory + path, fname),
                    buffering=1024 * 1024 * 16,
                    mode="rb",
                ) as f_in_raw:
                    with gzip.open(f_in_raw) as f_in:
                        shutil.copyfileobj(f_in, f_out)
                        # f_out.writelines(f_in)


def read_file(path):
    with open(path) as f:
        for line in tqdm(f):
            yield line


def download_files(link, output):
    import subprocess
    subprocess.run("wget -q -P " + output + " " + link,shell=True,check=True)


if __name__ == "__main__":
    file = sys.argv[1]
    get_ftp_links(file)
    download_ftp_files(file[:-4])
    unzip_files("/" + file[:-4])
