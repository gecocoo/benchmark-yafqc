from gecocoo.table.serialize import deserialize
import sys
import os
if __name__ == "__main__":
    source = sys.argv[1]
    print(f"loading table from {source}...")
    table, utils = deserialize(source)

    print(
        "table size:",
        utils.llconfig.num_buckets,
        "x",
        utils.llconfig.bucket_size,
        "=",
        utils.llconfig.num_buckets * utils.llconfig.bucket_size,
    )
    print("key count:", table.get_key_count(table.storage))
    print(
        "load factor:",
        table.get_key_count(table.storage)
        / (utils.llconfig.num_buckets * utils.llconfig.bucket_size),
    )
    print("storage size:", f"{table.storage.nbytes / 1024 ** 3:.4f} GiB")
    print("table metadata:", utils.metadata)
    print("hash functions:", len(utils.llconfig.hash_functions))

    if len(sys.argv) > 2:
        dest = sys.argv[2]
        print(f"saving compressed table to {dest}")
        with open(dest, mode="wb") as file:
            utils.serialize(file, compress=True)
        source_size = os.stat(source).st_size
        dest_size = os.stat(dest).st_size
        print(f"source file size: {source_size / 1024 ** 3:.4f} GiB")
        print(f"destination file size: {dest_size / 1024 ** 3:.4f} GiB")
        print(f"compression ratio: {dest_size / source_size:.3f}")
