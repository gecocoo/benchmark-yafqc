import os
from pathlib import Path


NODE_LOCAL_STORAGE = Path(os.environ["NODE_LOCAL_STORAGE"])
GLOBAL_STORAGE = Path(os.environ["GLOBAL_STORAGE"])

REF_GENOMES_DIR = GLOBAL_STORAGE / "ref_genomes"
REF_GENOMES_SINGLE = {
    "human.fna": "http://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa.gz",
}
REF_GENOMES_SINGLE_UNCOMPRESSED = {
    "phyx.fna": "ftp://ftp.ncbi.nlm.nih.gov/genomes/Viruses/enterobacteria_phage_phix174_sensu_lato_uid14015/NC_001422.fna",
}
REF_GENOMES_SUMMARY = {
    "bacteria.fna": ("https://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt", "--unique", "--max-taxid 100000"),
    "viral.fna": ("https://ftp.ncbi.nlm.nih.gov/genomes/refseq/viral/assembly_summary.txt",),
    "fungi.fna": ("https://ftp.ncbi.nlm.nih.gov/genomes/refseq/fungi/assembly_summary.txt",),
}
ALL_GENOME_FILES = [
    *[REF_GENOMES_DIR / "single" / name for name in REF_GENOMES_SINGLE.keys()],
    *[REF_GENOMES_DIR / "single_uncompressed" / name for name in REF_GENOMES_SINGLE_UNCOMPRESSED.keys()],
    *[REF_GENOMES_DIR / "summary" / name for name in REF_GENOMES_SUMMARY.keys()],
]

SAMPLES_DIR = GLOBAL_STORAGE / "samples"
SAMPLES = {
    "corona": "https://sra-download.ncbi.nlm.nih.gov/traces/sra65/SRR/012944/SRR13255543",
}

BUCKET_SIZES = [4, 8, 12]
KS = [25, 27, 31]
LOAD_FACTORS = [ 0.95]
HASH_FUNCTION_COUNTS = [2, 3, 4]
NS = {25: 5_984_845_496, 27: 6_031_233_027, 31: 6_261_140_111}

# ntCard results for huge bacteria set:
# NS = {25: 25_877_737_984, 27: 26_464_218_937, 31: 27_395_951_917}
# ntCard results for only unique organisms
# NS = {25: 23_515_119_770, 27: 23_974_774_046, 31: 24_669_148_649}

CONFIGS = list(
    [
        f"""
dimensions:
  n: {NS[k]}
  k: {k}
  bucket_size: {bucket_size}
  load_factor: {load_factor}

values:
  module_name: "yafqc.flags.flags_module"
  parameters:
    - {len(ALL_GENOME_FILES)}

hashfunctions:
  choices: {hash_function_count}
  default_hash: xorlinear
  functions:
    - default

storage:
  concurrent: true
        """
        for bucket_size in BUCKET_SIZES
        for load_factor in LOAD_FACTORS
        for hash_function_count in HASH_FUNCTION_COUNTS
        for k in KS
    ]
)


localrules:
    download_single_genome,
    download_single_genome_uncompressed,
    download_summary_genome,
    download_sample,
    generate_config,


rule all:
    input:
        [
            f"results/benchmarks-referencetables/config_{i}.tsv"
            for i in range(len(CONFIGS))
        ],
        [
            f"results/benchmarks-process/config_{i}-{sample}.tsv"
            for i in range(len(CONFIGS))
            for sample in SAMPLES.keys()
        ],
        [
            f"results/config_{i}-{sample}-report.json"
            for i in range(len(CONFIGS))
            for sample in SAMPLES.keys()
        ],


rule download_single_genome:
    output:
        REF_GENOMES_DIR / "single" / "{name}",
    params:
        url=lambda wildcards: REF_GENOMES_SINGLE[wildcards.name],
    log:
        "logs/download_single_genome_{name}.log"
    shell:
        "curl {params.url} 2> {log} | gunzip > {output}"

rule download_single_genome_uncompressed:
    output:
        REF_GENOMES_DIR / "single_uncompressed" / "{name}",
    params:
        url=lambda wildcards: REF_GENOMES_SINGLE_UNCOMPRESSED[wildcards.name],
    log:
        "logs/download_single_genome_{name}.log"
    shell:
        "curl {params.url} 2> {log} > {output}"

rule download_summary_genome:
    output:
        REF_GENOMES_DIR / "summary" / "{name}",
    params:
        args=lambda wildcards: " ".join(REF_GENOMES_SUMMARY[wildcards.name]),
    log:
        "logs/download_summary_genome_{name}.log"
    shell:
        "python3 download.py {params.args} > {output} 2> {log}"


rule download_sample:
    output:
        SAMPLES_DIR / "{name}.fastq",
    params:
        url=lambda wildcards: SAMPLES[wildcards.name],
    log:
        "logs/download_sample_{name}.log"
    shell:
        """
        curl {params.url} > {wildcards.name}.sradata 2> {log}
        env/bin/fastq-dump {wildcards.name}.sradata --stdout > {output} 2> {log}
        """


rule generate_config:
    output:
        "generated-configs/config_{i}.yaml",
    run:
        config_text = CONFIGS[int(wildcards.i)]
        os.makedirs(os.path.dirname(output[0]), exist_ok=True)
        with open(output[0], mode="w") as file:
            file.write(config_text)


rule copy_samples:
    input:
        [SAMPLES_DIR / f"{name}.fastq" for name in SAMPLES.keys()],
    output:
        touch("{config}-samples-copied.flag")
    params:
        dest_files=lambda wildcards: [ NODE_LOCAL_STORAGE / "samples" / f"{wildcards.config}_{name}.fastq" for name in SAMPLES.keys() ],
    group:
        "benchmark"
    run:
        import shutil

        for src, dst in zip(input, params.dest_files):
            os.makedirs(os.path.dirname(dst), exist_ok=True)
            shutil.copy(src, dst)


rule copy_reference_genomes:
    input:
        ALL_GENOME_FILES,
    output:
        touch("{config}-reference-genomes-copied.flag")
    params:
        dest_files=lambda wildcards: [NODE_LOCAL_STORAGE / f"{wildcards.config}_{file.name}" for file in ALL_GENOME_FILES],
    group:
        "benchmark"
    run:
        import shutil

        for src, dst in zip(input, params.dest_files):
            os.makedirs(os.path.dirname(dst), exist_ok=True)
            shutil.copy(src, dst)


rule benchmark_create_referencetables:
    input:
        "{config}-reference-genomes-copied.flag",
        config="generated-configs/{config}.yaml",
    output:
        touch("{config}-reference-table.flag"),
        cpu_info="results/{config}-cpu-reference-table.txt",
    params:
        reference_table=lambda wildcards: NODE_LOCAL_STORAGE / f"{wildcards.config}-reference.gecocoo",
        genomes=lambda wildcards: [ NODE_LOCAL_STORAGE / f"{wildcards.config}_{file.name}" for file in ALL_GENOME_FILES ]
    benchmark:
        repeat("results/benchmarks-referencetables/{config}.tsv", 10)
    group:
        "benchmark"
    log:
        "logs/{config}-reference-table.log",
    resources:
        mem_mb=60000,
    threads: 16
    shell:
        """
        lscpu > {output.cpu_info}
        yafqc contamination {params.genomes} --config {input.config} -o {params.reference_table} 2> {log}
        """


rule benchmark_process:
    input:
        "{config}-reference-table.flag",
        "{config}-samples-copied.flag",
    output:
        report="results/{config}-{sample}-report.json",
        report_html="results-local/{config}-{sample}-report.html",
        cpu_info="results/{config}-{sample}-cpu-process.txt",
    params:
        sample=lambda wildcards: NODE_LOCAL_STORAGE / "samples" / f"{wildcards.config}_{wildcards.sample}.fastq",
        reference_table=lambda wildcards: NODE_LOCAL_STORAGE / f"{wildcards.config}-reference.gecocoo",
    group:
        "benchmark"
    log:
        "logs/{config}-{sample}-process.log",
    resources:
        mem_mb=60000,
    threads: 16
    benchmark:
        repeat("results/benchmarks-process/{config}-{sample}.tsv", 10)
    shell:
        """
        lscpu > {output.cpu_info}
        yafqc process {params.sample} -r {params.reference_table} -s 4000 2> {log}  > {output.report}
        < {output.report} yafqc report > {output.report_html}
        """
