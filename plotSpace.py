import os
from strictyaml import load
import re
import sys
from dataclasses import dataclass
import csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import argparse


COLORS = ["#1b9e77", "#d95f02", "#7570b3"]
MARKERS = ["o", "s", "x"]
ALPHA = 0.5


@dataclass
class ResultData:
    k: int
    bucket_size: int
    number_hashfunctions: int
    seconds: float


def collect_configs(dir: str):
    files = [
        f
        for f in os.listdir(dir)
        if os.path.splitext(f)[1] == ".yaml" and f[:7] == "config_"
    ]
    file_array = np.empty((len(files)), dtype=np.object)
    for filename in files:
        try:
            name, _ = os.path.splitext(filename)
            number = int(name[7:])
            file_array[number] = filename
        except:
            print("failed conversion:", filename, file=sys.stderr)

    configs = []
    for i, filename in enumerate(file_array):
        with open(os.path.join(dir, filename)) as f:
            config = load(f.read())
            configs.append((i, config))

    return configs


def collect_results(results_dir: str):
    results = []
    results.append((0,	24031.97))
    results.append((1,	24218.24))
    results.append((2,	50282.85))
    results.append((3,	24031.97))
    results.append((4,	48436.48))
    results.append((5,	50282.85))
    results.append((6,	24031.97))
    results.append((7,	48436.48))
    results.append((8,	50282.85))
    results.append((9,	24031.97))
    results.append((10,	36327.36))
    results.append((11,	37712.14))
    results.append((12,	24031.97))
    results.append((13,	36327.36))
    results.append((14,	37712.14))
    results.append((15,	24031.97))
    results.append((16,	36327.36))
    results.append((17,	37712.14))
    results.append((18,	24031.97))
    results.append((19,	32290.99))
    results.append((20,	33521.90))
    results.append((21,	24031.97))
    results.append((22,	32290.99))
    results.append((23,	33521.90))
    results.append((24,	24031.97))
    results.append((25,	32290.99))
    results.append((26,	33521.90))
    return results


def plot(df: pd.DataFrame, title: str):
    fig, ax = plt.subplots()
    ks = list(sorted(df["k"].unique()))
    buckets = list(sorted(df["bucket_size"].unique()))
    hash_counts = list(sorted(df["number_hashfunctions"].unique()))
    k_pos = dict(zip(sorted(ks), [i * 11 for i in range(len(ks))]))
    max_time = max(df["seconds"])

    for i, bucket_group in enumerate(df.groupby("bucket_size")):
        color = COLORS[i]
        _, by_bucket = bucket_group

        for h, hash_group in enumerate(by_bucket.groupby("number_hashfunctions")):
            _, by_hash = hash_group
            y = list([r.seconds for r in by_hash.itertuples()])
            x = np.array([k_pos[r.k] + i * 3 + h for r in by_hash.itertuples()])

            ax.scatter(x=x, y=y, c=color, marker=MARKERS[h], alpha=ALPHA)

    ax.legend(
        handles=[
            *[
                mlines.Line2D(
                    [],
                    [],
                    marker=marker,
                    linestyle="None",
                    markersize=10,
                    label=f"hash count = {hash_count}",
                )
                for hash_count, marker in zip(hash_counts, MARKERS)
            ],
            *[
                mpatches.Patch(color=color, label=f"bucket size = {bucket_size}")
                for bucket_size, color in zip(buckets, COLORS)
            ],
        ],
        # loc="upper left",
    )

    ax.set_xticks(np.array(list(k_pos.values())) + 4.5)
    ax.set_xticklabels(np.array(list(k_pos.keys())))
    ax.set_ylim([0, max_time*1.1])
    ax.set_title(title)
    ax.set_ylabel("space (MiB)")
    ax.set_xlabel("k")

    return fig


if __name__ == "__main__":
    # if len(sys.argv) < 2:
    #     print("script requires a result_folder as argument to plot benchmarks", file=sys.stderr)
    #     exit(1)

    parser = argparse.ArgumentParser(description="Plot YAFQC benchmark results")
    parser.add_argument("results_dir", type=str, help="results directory")
    parser.add_argument("output_dir", type=str, help="output directory")
    parser.add_argument(
        "--type", dest="type", choices=("png", "pdf", "pgf"), default="pgf"
    )
    parser.add_argument("--usetex", dest="usetex", action="store_true", default=False)
    parser.add_argument("--dpi", dest="dpi", type=int, default=300)
    parser.add_argument("--width", dest="width", type=float, default=35)
    parser.add_argument("--height", dest="height", type=float, default=20)

    args = parser.parse_args()

    if args.usetex:
        plt.rcParams.update(
            {"pgf.rcfonts": False, "text.usetex": True, "font.family": "serif",}
        )


    # folder = sys.argv[1]
    p_result_dir = os.path.join(args.results_dir, "results/", "benchmarks-process")
    r_result_dir = os.path.join(args.results_dir, "results/", "benchmarks-referencetables")

    process_results = collect_results(p_result_dir)
    reference_results = collect_results(r_result_dir)

    config_dir = os.path.join(args.results_dir,"configs")
    configs = collect_configs(config_dir)
    ref_results = []
    for (i, pr), (j, config) in zip(reference_results, configs):
        assert i == j
        k = int(config.data["dimensions"]["k"])
        bucket_size = int(config.data["dimensions"]["bucket_size"])
        number_hashfunctions = int(config.data["hashfunctions"]["choices"])
        resultData = ResultData(k, bucket_size, number_hashfunctions, int(pr))
        ref_results.append(resultData)

    ref_results = pd.DataFrame(ref_results)
    fig_ref = plot(ref_results, "space of reference table")

    os.makedirs(args.output_dir, exist_ok=True)

    path = os.path.join(
        args.output_dir,
        f"referenceSpace.{args.type}",
    )
    fig_ref.savefig(path, dpi=args.dpi)
    plt.close(fig_ref)
    print(os.path.basename(path))

