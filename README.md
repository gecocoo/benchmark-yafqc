# Benchmark YAFQC on LIDO3

Tools for executing benchmarks on LiDO3. Results stored in this repository can be viewed at URL.

## Prerequisites

The script needs to be able to clone the gecocoo repository on the LiDO3 host.
It is therefore necessary to authenticate against GitLab.
This is possible via two options:

1. using SSH agent forwarding and thereby reusing the SSH key on your host
2. using a personal GitLab access token

Generate your personal GitLab access token at https://gitlab.com/-/profile/personal_access_tokens.
Make sure to check the scopes `read_repository` and `write_repository`.

## Setup on LiDO3

1.  Load the required modules:

    ```bash
    module load python/3.7.7
    module load git/2.28.0
    ```

    It's best to put them in your `.bashrc` for the future.

2.  Clone this repository into home directory:

    ```bash
    # Using ssh agent forwarding:
    git clone git@gitlab.com:gecocoo/benchmark-yafqc.git
    # Using your access token
    git clone https://oauth2:<GITLAB TOKEN HERE>@gitlab.com/gecocoo/benchmark-yafqc.git
    ```

3.  Set the URL to clone the gecocoo repository by:

    ```bash
    # Using ssh agent forwarding:
    export YAFQC_CLONE_URL=git@gitlab.com:gecocoo/yafqc.git
    # Using your access token
    export YAFQC_CLONE_URL=https://oauth2:<GITLAB TOKEN HERE>@gitlab.com/gecocoo/yafqc.git
    ```

    Possibly save this in a file, so you don't have to type this every time.

3.  Set your email address:

    ```bash
    export LIDO3_MAIL=max.mustermann@tu-dortmund.de
    ```

    Possibly save this in a file, so you don't have to type this every time.

4. Set the storage directories for LiDO3:

    ```bash
    export NODE_LOCAL_STORAGE=/scratch
    export GLOBAL_STORAGE=/work/$USER
    ```

## Running benchmarks


## Adding Results


