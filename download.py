from typing import Iterable
from urllib.request import urlopen
import gzip
from concurrent.futures import ThreadPoolExecutor, Future
from typing import IO, List
from threading import Lock
import sys
import argparse


def retrieve_assembly_urls(summary_url: str, max_taxid = None, only_unique_organisms=False, suffix: str = "_genomic.fna.gz"):
    with urlopen(summary_url) as request:
        charset = request.info().get_content_charset() or "utf-8"
        request.readline()  # skip first line

        # find column indices
        header = request.readline().decode(charset)
        assert header.startswith("# ")
        header_columns = header[2:].split("\t")
        assembly_accession_index = header_columns.index("assembly_accession")
        url_column_index = header_columns.index("ftp_path")
        asm_name_column_index = header_columns.index("asm_name")
        version_status_column_index = header_columns.index("version_status")
        assembly_level_column_index = header_columns.index("assembly_level")
        organism_name_column_index = header_columns.index("organism_name")
        infraspecific_name_column_index = header_columns.index("infraspecific_name")
        taxid_column_index = header_columns.index("taxid")

        # print column indices for debugging
        # print(list(enumerate(header_columns)), file=sys.stderr)

        unique_organisms = set()

        for line in request:
            columns = line.decode(charset).split("\t")
            # print(list(enumerate(columns)), file=sys.stderr)
            if len(columns) < max(
                assembly_accession_index,
                url_column_index,
                asm_name_column_index,
                version_status_column_index,
                assembly_level_column_index,
            ):
                continue
            assembly_accession = columns[assembly_accession_index]
            url = columns[url_column_index]
            asm_name = columns[asm_name_column_index]
            version_status = columns[version_status_column_index]
            assembly_level = columns[assembly_level_column_index]
            organism_name = columns[organism_name_column_index]
            taxid = columns[taxid_column_index]

            if max_taxid is not None and int(taxid) > max_taxid:
                continue

            # limit downloading to latest and complete genomes
            if version_status == "latest" and assembly_level == "Complete Genome" and not organism_name in unique_organisms:
                if only_unique_organisms:
                    unique_organisms.add(organism_name)
                # print(organism_name, infraspecific_name, file=sys.stderr)
                yield f"{url}/{assembly_accession}_{asm_name}{suffix}"


def download_and_concat_assemblies(file: IO, assembly_urls: List[str]):
    from concurrent.futures import as_completed

    with ThreadPoolExecutor(50) as downloader:
        write_lock = Lock()

        def download_and_write(url: str):
            with urlopen(url) as comprossed_file, gzip.open(
                comprossed_file
            ) as uncompressed_file:
                chunk = uncompressed_file.read()
            with write_lock:
                file.write(chunk)

        downloads = list(
            [downloader.submit(download_and_write, url) for url in assembly_urls]
        )
        success = 0
        failed = 0
        for download in as_completed(downloads):
            try:
                download.result()
                success += 1
            except Exception as err:
                print("error:", err, file=sys.stderr)
                failed += 1

            print(
                f"done with {success + failed}/{len(downloads)}: {success} successful, {failed} failed",
                file=sys.stderr,
            )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Download summary genomes')
    parser.add_argument('url', metavar="URL",
                        help='the assembly summary')
    parser.add_argument('--limit', metavar="n", dest='limit', type=int, default=None,
                        help='limit downloading to the first n genomes')
    parser.add_argument('--max-taxid', metavar="i", dest='max_taxid', type=int, default=None,
                        help='the maximum taxid to consider for downloading')
    parser.add_argument('--unique', action="store_true", dest="unique", default=False,
                        help='only consider unique organsisms for downloading')
    args = parser.parse_args()

    print("retrieving download urls...", file=sys.stderr)
    if args.max_taxid:
        print("max taxid to considered is", args.max_taxid, file=sys.stderr)
    if args.unique:
        print("only unique organisms are considered", file=sys.stderr)
    urls = list(retrieve_assembly_urls(args.url, max_taxid=args.max_taxid, only_unique_organisms=args.unique))
    print(f"got {len(urls)} urls to download", file=sys.stderr)

    if args.limit:
        urls = urls[:args.limit]
        print(f"limited urls to first {args.limit}", file=sys.stderr)

    download_and_concat_assemblies(sys.stdout.buffer, urls)