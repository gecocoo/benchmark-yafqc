# Comparison between FastQ-Screen and YAFQC
This Benchmark was a only used one time for the report and not executed on the HPC cluster.

## Reproduce
1. Use Snakemake to Aquire the references from the main Benchmark (change the rule all to `[[i for i in ALL_GENOME_FILES],]`)
2. Download FastQ-Screen
3. Download `SRR1501064.1.fastq` from NCBI
4. Modify the scripts `yafqc.sh` and `fqs.sh` to point to the directories you wish
5. Run the scripts one after the other and add upt the times from the steps

## Results
``
On a personal 4-Core machine. Times as real/user

	Building Table	 , Processing
Yafqc:	  7:30 /   18:30 , 14:37 /   41:27	
bt2+fqs: 44:42 / 2:25:19 , 28:25 / 1:42:24
``
