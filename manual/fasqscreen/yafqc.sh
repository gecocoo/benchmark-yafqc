#!/bin/sh
FASTQ_SCREEN_DIR=./FastQ-Screen-0.14.1	# The unpacked directory
THREADS=4			# Number of CPU Cores
FASTA_DIR=./bench_fasta		# Place where the Fasta-Files are
INDEX_DIR=./indices		# Place where the indices will be placed
OUT_DIR=./out			# Where to write the results
K=25				# K-Mer length
SIZE=2000000000 		# Size of the Table, ntCard gives a first estimate
TEST_FASTQ=./EBV-Human/SRR1501064.1.fastq	# The file to test
# head -n 7557416 for 1GiB
mkdir -p $OUT_DIR
mkdir -p $INDEX_DIR
if [ ! -f $INDEX_DIR/yafqc_table ]
then
(time yafqc contamination -t $THREADS -o $INDEX_DIR/yafqc_table -s $SIZE -k $K $FASTA_DIR/* ) 2>&1 | tee $OUT_DIR/time_yafqc_table_creation.txt
fi
(time yafqc process -s 4000 -t $THREADS -r $INDEX_DIR/yafqc_table $TEST_FASTQ > $OUT_DIR/yafqc.json ) 2>&1 | tee $OUT_DIR/time_yafqc_processing.txt
cat $OUT_DIR/yafqc.json | yafqc report > $OUT_DIR/yafqc_report.html
