#!/bin/sh
FASTQ_SCREEN_DIR=./FastQ-Screen-0.14.1	# The unpacked directory
THREADS=4			# Number of CPU Cores
FASTA_DIR=./bench_fasta		# Place where the Fasta-Files are
INDEX_DIR=./indices		# Place where the indices will be placed
OUT_DIR=./out			# Where to write the results
TEST_FASTQ=./SRR1501064.1.fastq	# The file to test
NAMES=(	#Name		Fasta Filename
	"bacteria bacteria.fna"
	"fungi fungi.fna"
	"human human.fna"
	"phyx phyx.fna"
	"viral viral.fna"
)
mkdir -p $OUT_DIR
mkdir -p $INDEX_DIR
if [ ! -f $FASTQ_SCREEN_DIR/fastq_screen.conf ]
then
	cp $FASTQ_SCREEN_DIR/fastq_screen.conf.example $FASTQ_SCREEN_DIR/fastq_screen.conf
fi
for i in "${NAMES[@]}"
do
	NAME=$(echo $i | cut -f 1 -d " ")
	FASTA=$(echo $i | cut -f 2 -d " ")
	echo Name: $NAME
	echo Fasta: $FASTA
	if [ ! -f $INDEX_DIR/$NAME.1.bt2 ]
	then
		(time bowtie2-build --threads $THREADS $FASTA_DIR/$FASTA $INDEX_DIR/$NAME > $INDEX_DIR/log_bt2_$NAME) 2> $OUT_DIR/time_bt2_$NAME.txt
		$FASTQ_SCREEN_DIR/fastq_screen --conf $FASTQ_SCREEN_DIR/fastq_screen.conf  --add_genome $NAME,$INDEX_DIR/$NAME,$NAME
	fi
done
(time $FASTQ_SCREEN_DIR/fastq_screen --threads $THREADS --conf $FASTQ_SCREEN_DIR/fastq_screen.conf --subset 0 --aligner bowtie2 --outdir $OUT_DIR $TEST_FASTQ ) 2>&1 | tee $OUT_DIR/time_fastq_screen.txt
