#!/bin/bash
set -e

CONDA_DOWNLOAD="https://repo.anaconda.com/miniconda/Miniconda3-py37_4.9.2-Linux-x86_64.sh"
CONDA_SHA256="79510c6e7bd9e012856e25dcb21b3e093aa4ac8113d9aa7e82a86987eabe1c31"
CONDA_TARGET="conda.tmp.sh"

curl "$CONDA_DOWNLOAD" -o $CONDA_TARGET
if [ "$(sha256sum $CONDA_TARGET | cut -f1 -d " ")" != $CONDA_SHA256 ]; then
    echo "conda checksum is invalid!"
    exit 1
fi
bash $CONDA_TARGET -b
rm $CONDA_TARGET
