from setuptools import setup, find_packages

NAME = "yafqc"
PACKAGE_NAME = "yafqc"

# load and set VERSION and DESCRIPTION
vcontent = open(f"{PACKAGE_NAME}/_version.py").read()
exec(vcontent)

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            f"{NAME} = {PACKAGE_NAME}.__init__:main",
        ],
    },
    install_requires=[
        "numba",
        "numpy",
        "gecocoo",
    ],
    license="MIT",
)
