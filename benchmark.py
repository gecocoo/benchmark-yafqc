#!/usr/bin/env python3

import subprocess
import os
import shutil
import sys
import pwd
import datetime
import re
from socket import IPV6_JOIN_GROUP, gethostname

# 1. Clone / Checkout yafqc project at commit
# 2. Install dependencies
# 3. Copy Snakefile, scripts etc. to work directory
# 4. Execute snakemake which in turn creates multiple SLURM jobs
# 4. Copy results to target directory

# Check if "lido" is contained in the system's host name
RUNNING_ON_LIDO3 = "lido" in gethostname().lower()
BENCHMARKS_DIR = os.path.dirname(__file__)
YAFQC_CLONE_URL = os.environ.get("YAFQC_CLONE_URL")
LIDO3_MAIL = os.environ.get("LIDO3_MAIL")


def checkout_yafqc(commit: str, destination: str) -> str:
    print(f"cloning yafqc to {destination}")
    subprocess.run(
        ["git", "clone", "--recurse-submodules", YAFQC_CLONE_URL, destination]
    ).check_returncode()

    print(f"checking out to revision {commit}")
    subprocess.run(
        ["git", "checkout", "--detach", commit], cwd=destination
    ).check_returncode()

    result = subprocess.run(
        ["git", "rev-parse", "HEAD"], cwd=destination, capture_output=True, text=True
    )
    result.check_returncode()
    return result.stdout.strip()


def install(work_dir: str, checkout_dir: str):
    # Install the dependencies from requirements.txt

    # create conda env
    subprocess.run(
        [
            # os.path.join(work_dir),
            "conda",
            "env",
            "create",
            "--prefix",
            os.path.join(work_dir, "env"),
            "-f",
            os.path.join(BENCHMARKS_DIR, "environment.yml"),
        ],
        check=True,
    )

    # Install gecocoo
    subprocess.run(
        [os.path.join(work_dir, "env", "bin", "pip"), "install", "-e", os.path.join(checkout_dir, "gecocoo")]
    ).check_returncode()

    # Install yafqc itself
    subprocess.run(
        [os.path.join(work_dir, "env", "bin", "pip"), "install", "-e", checkout_dir]
    ).check_returncode()


def copy_benchmark_files(work_dir: str):
    files = ["Snakefile", "download.py"]
    directories = ["slurm"]

    for file in files:
        shutil.copy(os.path.join(BENCHMARKS_DIR, file), os.path.join(work_dir, file))

    for dir in directories:
        shutil.copytree(os.path.join(BENCHMARKS_DIR, dir), os.path.join(work_dir, dir))


def run_snakemake(work_dir: str):
    max_concurrent = 50
    subprocess.run(
        [
            "env/bin/snakemake",
            f"-j={max_concurrent}",
            *(["--profile=slurm"] if RUNNING_ON_LIDO3 else []),
        ],
        cwd=work_dir,
        check=True,
    )



def copy_results(work_dir: str, commit: str, timestamp: str):
    results_dir = os.path.join(BENCHMARKS_DIR, "results", commit, timestamp)
    os.makedirs(results_dir)

    print(f"copying the benchmark results to {results_dir}")
    shutil.copytree(os.path.join(work_dir, "generated-configs"), os.path.join(results_dir, "configs"))
    shutil.copytree(os.path.join(work_dir, "results"), os.path.join(results_dir, "results"))
    shutil.copytree(os.path.join(work_dir, "logs"), os.path.join(results_dir, "logs"))



def get_username():
    return pwd.getpwuid(os.getuid()).pw_name


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("script requires a revision to benchmark as argument", file=sys.stderr)
        exit(1)

    if YAFQC_CLONE_URL is None:
        print("YAFQC_CLONE_URL must be set")
        exit(1)

    if LIDO3_MAIL is None:
        print("LIDO3_MAIL for email notifications must be set")
        exit(1)

    revision = sys.argv[1]
    escaped_revision = revision.replace("/", "_")
    if RUNNING_ON_LIDO3:
        base_work_dir = os.path.join("/work", get_username())
    else:
        base_work_dir = os.path.join("./work", get_username())
    timestamp = datetime.datetime.now().isoformat().replace(":", "_")
    work_dir = os.path.join(base_work_dir, f"yafqc-{timestamp}-{escaped_revision}")
    checkout_dir = os.path.join(work_dir, "yafqc")
    print(f"setting up benchmarking of revision {revision} in {work_dir}")

    commit = checkout_yafqc(revision, checkout_dir)
    print(f"benchmarking commit {commit}")

    install(work_dir, checkout_dir)

    copy_benchmark_files(work_dir)
    run_snakemake(work_dir)
    copy_results(work_dir, commit, timestamp)
